# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.12 1.11 ] ]

export_exlib_phases src_prepare

SUMMARY="An almost exact clone of ncmpc"
DESCRIPTION="
Ncmpcpp contains some new features ncmpc doesn't have. It's been also rewritten
from scratch in C++. New features include:
* tag editor
* playlists editor
* easy to use search screen
* media library screen
* lyrics screen
* audio output selection screen
* possibility of going to any position in currently playing track without rewinding/fastforwarding
* multi colored main window (if you want)
* songs can be added to playlist more than once
* a lot of minor useful functions
"
HOMEPAGE="http://unkart.ovh.org/ncmpcpp/"

BUGS_TO="alip@exherbo.org"
LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    curl [[ description = [ Enable fetching lyrics ] ]]
    taglib [[ description = [ Enable tag editor ] ]]
    visualizer [[ description = [ Enable frequency spectrum visualization ] ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/boost
        media-libs/libmpdclient[>=2.1]
        sys-libs/ncurses
        curl? ( net-misc/curl )
        taglib? ( media-libs/taglib )
        visualizer? ( sci-libs/fftw )
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( visualizer )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( curl taglib 'visualizer fftw')
DEFAULT_SRC_CONFIGURE_PARAMS=( '--enable-clock' '--enable-outputs' '--without-iconv' )

ncmpcpp_src_prepare() {
    edo sed -i \
        -e "s:\$(prefix)/share/doc/\$(PACKAGE):/usr/share/doc/${PNV}:" \
        -e "/^doc_DATA/s:COPYING::" \
        Makefile.am doc/Makefile.am
    autotools_src_prepare
}

